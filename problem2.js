function getLastCarMakeAndModel(inventory){
    if (Array.isArray(inventory) && inventory.length > 0){
        const lastCar = inventory[inventory.length - 1];
        return `last car is a ${lastCar.car_make} ${lastCar.car_model}`;
    }
    else {
        return " ";
    }
}
module.exports = getLastCarMakeAndModel;