// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

// Creating A Function Name alphabeticalOrder with one argument

function alphabeticalOrder(inventory) {
    for (let i = 0; i < inventory.length - 1; i++) {
      for (let j = 0; j < inventory.length - i - 1; j++) {
        if (
          inventory[j].car_model.toUpperCase() >
          inventory[j + 1].car_model.toUpperCase()
        ) {
          // Swap elements if they are in the wrong order
          const temp = inventory[j];
          inventory[j] = inventory[j + 1];
          inventory[j + 1] = temp;
        }
      }
    }
    // creating an empty to store sorted Array of car_model
  
    let sortedarray = [];
    for (let i = 0; i < inventory.length; i++) {
      sortedarray.push(inventory[i].car_model);
    }
    return sortedarray;
  }
  
  // creating module to export the above function
  
  module.exports = alphabeticalOrder;
  