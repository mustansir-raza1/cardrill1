function getCarModels(inventory,car_model){
    
    return inventory.sort((a, b) => a.car_model.localeCompare(b.car_model));
}
module.exports = getCarModels;