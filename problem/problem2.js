function getLastCarMakeAndModel(inventory) {
    const lastCar = inventory[inventory.length - 1];
    return {
        make: lastCar.car_make,
        model: lastCar.car_model
    };
}

module.exports = getLastCarMakeAndModel;