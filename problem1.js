/* find the car which has an id 33*/
function findCarById (inventory){
    if (Array.isArray(inventory)){
        for(let index = 0; index < inventory.length; index++){
            if(inventory[index].id == 33){
                return inventory[index];
            }
        }
    }
    else {
        return [" "];
    }
}
module.exports = findCarById;