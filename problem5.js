function findOlderCars(inventory) {
    if(Array.isArray(inventory)){
        let olderCars = [];
        for(let i = 0; i < inventory.length; i++){
            if(inventory[i].car_year < 2000){
                olderCars.push(inventory[i]);
            }
        }
        console.log("Number of cars older than the year 2000: ",olderCars.length);
        console.log("Older cars:", olderCars);
    }else{
        return [""];
    }
}
module.exports = findOlderCars;