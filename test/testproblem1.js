// problem test

const findCarById = require('../problem1');
const inventoryData = require('../inventory');

try{
    const car = findCarById(inventoryData);
    if (car) {
        console.log(`Car 33 is a ${car.car_year} ${car.car_make} ${car.car_model}`);
    } 
    else{
        console.log("Car with ID 33 not found.");
    }
}
catch(error){
    console.log("error")
}
