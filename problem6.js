function getBmwAndAudi(inventory){
    if(Array.isArray(inventory)){
        let result = [];
        for(let i = 0; i < inventory.length; i++){
            let car = inventory[i];
            if(car.car_make ==="BMW" || car.car_make === "Audi"){
                result.push(car.car_make);
            }
        }
        return result;
    }
    else{
        return [""];
    }
}
module.exports = getBmwAndAudi;